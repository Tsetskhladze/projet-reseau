#!/usr/bin/env python
import socket
import sys
#python version 2.7.14+
def port():
    print("Quele port voulez vous connecter")
    port=raw_input()
    return int(port)

def ip():
    print("Quele IP address voulez vous connecter")
    ip=raw_input()
    return ip

def print_data(data):
    print>>sys.stderr, 'nous auron recu %s' % data

def get_data(conn):
    """
    Recevoir des donnees depuis le serveur
    """
    data=conn.recv(8192)
    if data == "":
        print("Connection closed")
        exit()
    return (data)

def get_request():
    """
    Recuperer a la ligne de commande le choix de l'utilisateur
    """
    method=raw_input()
    if method == None:
        return None
    else:
        return method
def send_choice(chc,sock):
    """
    Envoie le choix de l'utilisateur au serveur
    """
    sock.sendall(chc)

port=port()
ip=ip()
sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address=(ip,port)
print>>sys.stderr, 'Connexion vers %s sur le port %s'% server_address
sock.connect(server_address)

while True:
    data=get_data(sock)
    print_data(data)
    request=get_request()
    send_choice(req,sock)
